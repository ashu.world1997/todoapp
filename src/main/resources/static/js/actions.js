
function validation(){
	var title = document.getElementById('title').value.trim();
	var desc = document.getElementById('desc').value.trim();
	var date = document.getElementById('date').value.trim();
	var priority = document.getElementById('priority').value;
	
	var isValid=true;
	var reason;
	if(typeof(title) =='undefined' || title ==''){
	  isValid=false;	
	  reason='Title can\'t be empty';
	}
	else if(typeof(desc) =='undefined' || desc ==''){
		isValid=false;	
		reason='Desciption can\'t be empty';
	}
	else if(typeof(date) =='undefined' || date =='' ||invalidDate(date)){
		isValid=false;	
		reason='Invalid Date';
	}
	else if(typeMismatch(priority)){
		isValid=false;	
		reason='Priority should be an integer';
	}
	if(!isValid){
		document.getElementById('error').innerHTML=reason;
		return false ;
	}
	
		
	return true;
}

function typeMismatch(priority){
	if(typeof(priority) !='undefined' && priority !=''){
		return !(/^[1-10]$/).test(priority);
	}
	return false;
}
function invalidDate(formdate){
	if(new Date(formdate).setHours(0,0,0,0) < new Date().setHours(0,0,0,0)){
		return true;
	}
}

//validation for MM/dd/yyyy
function invalidRegex(date){
	var regex=/^((0?[1-9]|1[012])[/](0?[1-9]|[12][0-9]|3[01])[/](19|20)?[0-9]{2})$/;
	return !(regex.test(date));
}