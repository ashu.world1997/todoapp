package com.myproject.UserDaily;

import java.time.format.DateTimeFormatter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;

@SpringBootApplication
public class UserDailyApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserDailyApplication.class, args);
	}
	
	@Bean
    public Java8TimeDialect java8TimeDialect() {
        return new Java8TimeDialect();
    }
//	
//	@Bean
//    public FormattingConversionService conversionService() {
//        DefaultFormattingConversionService conversionService = 
//          new DefaultFormattingConversionService(false);
// 
//        DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();
//        registrar.setDateFormatter(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
//        registrar.setDateTimeFormatter(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
//        registrar.registerFormatters(conversionService);
// 
//        // other desired formatters
// 
//        return conversionService;
//    }

}
