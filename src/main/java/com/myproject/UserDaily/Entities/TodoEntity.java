package com.myproject.UserDaily.Entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="todo")
public class TodoEntity  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5286663957025359391L;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="title")
	private String title;
	@Column(name="description")
	private String description;
	@Column(name="priority")
	private Integer priority;
	
	//@Temporal(TemporalType.DATE)
	@Column(name="doc")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate doc;
	
	public TodoEntity() {
		// TODO Auto-generated constructor stub
	}
	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public LocalDate getDoc() {
		return doc;
	}

	public void setDoc(LocalDate doc) {
		this.doc = doc;
	}
	
}
