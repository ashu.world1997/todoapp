package com.myproject.UserDaily.TodoDao;

import java.util.List;

import com.myproject.UserDaily.Entities.TodoEntity;

public interface TodoDao {
	
	List<TodoEntity> findAllTodo();
	
	void saveOrUpdate(TodoEntity todo);
	
	void deleteTodo(Integer id);
	
	TodoEntity getTodo(Integer id);

}
