package com.myproject.UserDaily.TodoDao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myproject.UserDaily.Entities.TodoEntity;
import com.myproject.UserDaily.Respository.TodoRepository;

@Service
public class TodoDaoServiceImpl implements TodoDao{

	@Autowired
	TodoRepository todorepo;
	
	@Override
	public List<TodoEntity> findAllTodo() {
	
		return todorepo.findAllOrderByDocDesc();
		
	}

	@Override
	public void saveOrUpdate(TodoEntity todo) {
		
       todorepo.save(todo);
		
	}

	@Override
	public void deleteTodo(Integer id) {
		Optional<TodoEntity> todo = todorepo.findById(id);
		if(!todo.isPresent()) {
			System.out.println("No Such Entity Present");
		}
		
		todorepo.delete(todo.get());
		
	}

	@Override
	public TodoEntity getTodo(Integer id) {
		Optional<TodoEntity> todo=todorepo.findById(id);
		if(todo.isPresent()) {
			return todo.get();
		}
		return null;
	}
	
	

}
