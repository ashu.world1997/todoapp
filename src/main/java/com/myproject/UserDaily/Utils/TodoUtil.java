package com.myproject.UserDaily.Utils;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.myproject.UserDaily.Entities.TodoEntity;

public class TodoUtil {
	
	private static final Map<Integer,Integer> dayToSunday;
	static {
		Map<Integer,Integer> map=new HashMap<Integer, Integer>();
		map.put(0,7);
		map.put(1,6);
		map.put(2,5);
		map.put(3,4);
		map.put(4,3);
		map.put(5,2);
		map.put(6,1);
		dayToSunday=Collections.unmodifiableMap(map);
	}
	
	public static List<TodoEntity> itemThisWeek(List<TodoEntity> todoList){
		List<TodoEntity> todo=new ArrayList<TodoEntity>();
		Integer dayleft=dayToSunday.get(LocalDate.now().getDayOfWeek().getValue()-1); 
		if(dayleft == null) {
			return todo;
		}
		todoList.stream().forEach(task->{
		long diff=Period.between(LocalDate.now(), task.getDoc()).getDays();
		System.out.println(diff+" "+dayleft);
		if(diff>=0 && diff<=dayleft) {
			todo.add(task);
			System.out.println(task.getTitle());
		}
		});
		if(todo.size()==0) {
			TodoEntity t= new TodoEntity();
			t.setTitle("No Items to Display");
			todo.add(t);
		}
		return todo;
	}
	
	public static long betweenDates(Date firstDate, Date secondDate) {
		
	    return ChronoUnit.DAYS.between(firstDate.toInstant(), secondDate.toInstant());
	}

}
