package com.myproject.UserDaily.Controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.myproject.UserDaily.Entities.TodoEntity;
import com.myproject.UserDaily.TodoDao.TodoDaoServiceImpl;
import com.myproject.UserDaily.Utils.TodoUtil;

@Controller
@RequestMapping("/todo")
public class TodoController {
	
	@Autowired
	TodoDaoServiceImpl todoservice;
	
	@GetMapping("")
	public String todohome(Model model) {
		TodoEntity td = new TodoEntity();
		//td.setDoc(LocalDate.now());
		model.addAttribute("todo", td);
		List<TodoEntity> todolist=todoservice.findAllTodo();
		List<TodoEntity> weektodo=TodoUtil.itemThisWeek(todolist);
		System.out.println(weektodo.size());
		model.addAttribute("weektodo", weektodo);
		model.addAttribute("todolist",todolist );
		return "todo";
	}
	
	@PostMapping("/submitTodo")
	public String displayTodoLists(@ModelAttribute("todo") TodoEntity todo,Model model){
		todoservice.saveOrUpdate(todo);
	   System.out.println(todo.getTitle()+todo.getDescription()+todo.getDoc()+todo.getPriority());
		return "redirect:/todo";
	}
	
	@PostMapping("/delete")
	public String removeTodo(@RequestParam("todoId") Integer id) {
		todoservice.deleteTodo(id);
		return "redirect:/todo";
	}
	
	@GetMapping("/update")
	public String updateView(@RequestParam("todoId") Integer id,Model theModel) {
		System.out.println(id);
		TodoEntity td= todoservice.getTodo(id);
		theModel.addAttribute("todo",td);	
		return "update";
	}
	
	@PostMapping("/updateTodo")
	public String updateTodo(@ModelAttribute("todo") TodoEntity todo,Model model){
		todoservice.saveOrUpdate(todo);
	   System.out.println(todo.getTitle()+todo.getDescription()+todo.getDoc()+todo.getPriority());
		return "redirect:/todo";
	}
	
	
	
}
