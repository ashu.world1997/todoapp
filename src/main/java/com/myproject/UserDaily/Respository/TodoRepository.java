package com.myproject.UserDaily.Respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.myproject.UserDaily.Entities.TodoEntity;

public interface TodoRepository extends JpaRepository<TodoEntity, Integer>{

	@Query("select todo from TodoEntity todo order by todo.doc")
	List<TodoEntity> findAllOrderByDocDesc();
}
